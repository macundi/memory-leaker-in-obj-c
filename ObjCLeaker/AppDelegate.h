//
//  AppDelegate.h
//  ObjCLeaker
//
//  Created by Markus Stöbe on 07.12.16.
//  Copyright © 2016 Markus Stöbe. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

