//
//  main.m
//  ObjCLeaker
//
//  Created by Markus Stöbe on 07.12.16.
//  Copyright © 2016 Markus Stöbe. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
	return NSApplicationMain(argc, argv);
}
