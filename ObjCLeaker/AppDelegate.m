//
//  AppDelegate.m
//  ObjCLeaker
//
//  Created by Markus Stöbe on 07.12.16.
//  Copyright © 2016 Markus Stöbe. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
	// Insert code here to initialize your application
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
	// Insert code here to tear down your application
}


@end
