//
//  ViewController.m
//  ObjCLeaker
//
//  Created by Markus Stöbe on 07.12.16.
//  Copyright © 2016 Markus Stöbe. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

- (void)viewDidLoad {
	[super viewDidLoad];

	// Do any additional setup after loading the view.
	self.listOfObjects = [self createRingOfSize:5];
}


- (void)setRepresentedObject:(id)representedObject {
	[super setRepresentedObject:representedObject];

	// Update the view, if already loaded.
}

- (IBAction)leakIt:(id)sender {
	self.listOfObjects = nil;
}

- (DummyObject*) createRingOfSize:(int)size {
	DummyObject *firstDummy,*lastDummy;
	
	for (int i=0; i < size; i++) {
		DummyObject *newDummy = [[DummyObject alloc] init];
		
		if (i==0) {
			firstDummy = newDummy;
		} else {
			lastDummy.next = newDummy;
		}
		
		lastDummy = newDummy;
	}
	
	__weak DummyObject *weakLink = firstDummy;
	
	lastDummy.next = weakLink;
	return firstDummy;
}

@end
