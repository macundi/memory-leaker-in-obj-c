//
//  DummyObject.h
//  ObjCLeaker
//
//  Created by Markus Stöbe on 07.12.16.
//  Copyright © 2016 Markus Stöbe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DummyObject : NSObject

@property DummyObject *next;

@end
