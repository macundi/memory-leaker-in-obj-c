//
//  ViewController.h
//  ObjCLeaker
//
//  Created by Markus Stöbe on 07.12.16.
//  Copyright © 2016 Markus Stöbe. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DummyObject.h"

@interface ViewController : NSViewController

@property DummyObject *listOfObjects;

- (DummyObject*) createRingOfSize:(int)size;

@end

